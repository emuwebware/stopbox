#!/usr/bin/python
import ConfigParser
import sys, os
import argparse

# Settings types
SETTING_TYPE_BOOLEAN = 1
SETTING_TYPE_INT = 2
SETTING_TYPE_FLOAT = 3
SETTING_TYPE_STRING = 4

def main():

  parser = argparse.ArgumentParser()
  parser.add_argument('-s', '--settings', help='Settings file', type=str)
  args = parser.parse_args()
      
  settings = Settings(args.settings)

class Settings:
  
  def __init__(self, settings_file):
      
    self.settings_file = settings_file
    self.load_settings()

  def reload_settings(self):
    self.load_settings()

  def load_settings(self):

    self.settings = ConfigParser.SafeConfigParser()
    self.settings.read(self.settings_file)

    print("Reading from settings file: " + self.settings_file)

    self.interval                   = self.get_setting('animation', 'interval', SETTING_TYPE_INT, 300)
    self.output_dir                 = self.get_setting('animation', 'output_dir', SETTING_TYPE_STRING, '/home/pi/animations')
    self.ani_name                   = self.get_setting('animation', 'name', SETTING_TYPE_STRING, 'ani_1')
    self.preview_window_x             = self.get_setting('preview', 'x', SETTING_TYPE_INT, 40)
    self.preview_window_y             = self.get_setting('preview', 'y', SETTING_TYPE_INT, 220)
    self.preview_window_width             = self.get_setting('preview', 'width', SETTING_TYPE_INT, 800)
    self.preview_window_height             = self.get_setting('preview', 'height', SETTING_TYPE_INT, 600)

  def update_settings(self, section, name, settings_value):
    if self.settings.has_section(section):
      self.settings.set(section, name, str(settings_value))
      self.write_settings()
      
  def get_setting(self, section, name, settings_type, default):
    try:
      if settings_type == SETTING_TYPE_BOOLEAN:
        return self.settings.getboolean(section,name)
      elif settings_type == SETTING_TYPE_FLOAT:
        return self.settings.getfloat(section,name)
      elif settings_type == SETTING_TYPE_INT:
        return self.settings.getint(section,name)
      else:
        return self.settings.get(section,name)
    except (ConfigParser.NoSectionError, ConfigParser.NoOptionError):
      if not self.settings.has_section(section):
        self.settings.add_section(section)
      self.settings.set(section, name, str(default))
      self.write_settings()
      print("Adding missing settings: " + str(name) + " with default " + str(default))
      return default
  
  def write_settings(self):
    with open(self.settings_file, 'wb') as file_handle:
      self.settings.write(file_handle)
      

if __name__ == '__main__':
    main()