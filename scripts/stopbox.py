#!/usr/bin/python

from gpiozero import Button
from picamera import PiCamera
from datetime import datetime
from signal import pause
from settings import Settings
import os
import json
import time
import sys
import argparse

def main():

  parser = argparse.ArgumentParser()
  parser.add_argument('-s', '--settings', help='Settings file', type=str)
  args = parser.parse_args()
  
  stop_box = StopBox(args.settings)
  stop_box.start_recording()

# --------------------------------------------------------------------------------

class StopBox:

  def __init__(self, settings_file):
      
    self.settings_file = settings_file
    self.settings = Settings(settings_file)
    self.create_directories()    
    self.setup_controls()
    self.setup_camera()

  def create_directories(self):

    if not self.settings.output_dir.endswith("/"):
      self.settings.output_dir = self.settings.output_dir + "/"

    if not os.path.exists(self.settings.output_dir):
      os.makedirs(self.settings.output_dir)

    self.frames_path = self.settings.output_dir + self.settings.ani_name

    if not os.path.exists(self.frames_path):
      os.makedirs(self.frames_path)

  def setup_controls(self):
    self.button = Button(18)
  
  def setup_camera(self):
    self.camera = PiCamera()
    self.camera.resolution = (1024,768)

  def capture_frame(self):
    timestamp = datetime.now().strftime("%Y-%m-%d-%H-%M-%S")
    self.camera.capture(self.frames_path + '/' + timestamp + '.png')
    self.log({"type":"file", "data": timestamp + ".png"})

  def start_recording(self):
    self.show_preview()
    try:
      self.button.when_pressed = self.capture_frame
      pause()
    except KeyboardInterrupt:
      self.stop_preview()
      # os.system('convert -delay 50 -loop 0 ./animations/*.jpg ./animations/animation.gif')
      # print("Done")

  def stop_preview(self):
    self.camera.stop_preview()

  def show_preview(self):
    self.camera.start_preview(fullscreen=False, window=(self.settings.preview_window_x,self.settings.preview_window_y,self.settings.preview_window_width,self.settings.preview_window_height))

  def log(self, data):
    print(json.dumps(data))
    sys.stdout.flush()

# ------------------------------------------------------------------------------

if __name__ == '__main__':
    main()





