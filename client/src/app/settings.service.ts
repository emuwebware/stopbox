import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Settings } from './interfaces/settings';

interface SettingsFileResponse {
  settingsFile: string
}

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  public settings: BehaviorSubject<Settings> = new BehaviorSubject(null)
  public settingsFile: BehaviorSubject<string> = new BehaviorSubject(null)

  constructor(
    public http: HttpClient,        
  ) { 
    this.getSettings().then((settings) => {
      console.log("Settings", settings);
    }, (err) => {
      console.log("Unable to get settings");
    })
    this.getSettingsFile().then((settingsFile) => {
      console.log("Settings File", settingsFile);
    }, (err) => {
      console.log("Unable to get settings file");
    })
  }

  updateSettings(settings: Settings) {
    return this.http
      .post(`/api/settings`, settings)
      .toPromise().then((response: Settings) => {
        this.settings.next(response);
      });
  }

  getSettings() {
    return this.http
      .get<Settings>(`/api/settings`)
      .map((response) => {
        this.settings.next(response)
        return response;
      }).toPromise();        
  }

  getSettingsFile(): Promise<string> {
    return this.http
      .get<SettingsFileResponse>(`/api/settings-file`)
      .map((response) => {
        this.settingsFile.next(response.settingsFile)
        return response.settingsFile;
      }).toPromise();        
  }

  updateSettingsFile(settingsFile: string): Promise<boolean> {
    return this.http
      .post<SettingsFileResponse>(`/api/settings-file`, { settingsFile: settingsFile } )
      .map((response) => {
        this.settingsFile.next(response.settingsFile)
        this.getSettings();
        return true;
      }).toPromise();        
  }

}
