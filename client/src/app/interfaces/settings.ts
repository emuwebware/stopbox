export interface Settings {
  animation: {
    interval: any
    output_dir: string
    name: string
  },
  preview: {
    x: number
    y: number
    width: number
    height: number
  }
}
