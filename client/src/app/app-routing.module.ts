import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AnimationPageComponent } from './build/animation-page/animation-page.component';

const routes: Routes = [
  { path: '', redirectTo: 'build', pathMatch: 'full' },
  { path: 'build', component: AnimationPageComponent },  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
