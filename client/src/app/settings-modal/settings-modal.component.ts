import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { SettingsService } from '../settings.service';
import { Settings } from '../interfaces/settings';
import { Subject } from 'rxjs';
import { AnimationService } from '../build/animation.service';

@Component({
  selector: 'app-settings-modal',
  templateUrl: './settings-modal.component.html',
  styleUrls: ['./settings-modal.component.scss']
})
export class SettingsModalComponent implements OnInit, OnDestroy {

  public settings: Settings
  private _componentDestroyed: Subject<boolean> = new Subject<boolean>();  
  public settingsFile:string

  constructor(
    public activeModal: NgbActiveModal,    
    public settingsService: SettingsService,
    private _aniService: AnimationService,
  ) { }

  ngOnInit(): void {
    this.settingsService.settings.takeUntil(this._componentDestroyed.asObservable())
      .subscribe(settings => {
        this.settings = settings
      })
    this.settingsService.settingsFile.takeUntil(this._componentDestroyed.asObservable())
      .subscribe(settingsFile => {
        this.settingsFile = settingsFile
      })
  }

  updateSettingsFile() {
    this.settingsService.updateSettingsFile(this.settingsFile).then(() => {
      console.log("Settings file updated");
    }, (err) => {
      console.log("Unable to save settings file", err);
    });
  }
  
  save() {
    this.settingsService.updateSettings(this.settings).then(() => {
      console.log("Settings file saved");
    }, (err) => {
      console.log("Unable to save settings file", err);
    });
  }

  ngOnDestroy() {
    this._componentDestroyed.next(true);
    this._componentDestroyed.complete();    
  }

}
