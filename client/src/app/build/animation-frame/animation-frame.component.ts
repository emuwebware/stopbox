import { Component, OnInit, Input, OnDestroy, AfterContentInit, HostBinding } from '@angular/core';
import { AnimationService } from '../animation.service';
import { Subject } from 'rxjs';
import 'rxjs/add/operator/takeUntil';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'animation-frame',
  templateUrl: './animation-frame.component.html',
  styleUrls: ['./animation-frame.component.scss']
})
export class AnimationFrameComponent implements OnInit, OnDestroy, AfterContentInit {

  @Input()
  frame: string;
  
  @Input()
  path: string;

  @Input()
  frameIndex: number;

  @HostBinding('class.active') 
  active: boolean = false;

  private _currentFrameIndex: number
  private _componentDestroyed: Subject<boolean> = new Subject<boolean>();
  public frameSrc: string

  constructor(  
    private _animationService: AnimationService,
  ) { }

  ngAfterContentInit() {
    this.frameSrc = `/api/animation/frame/${this.frame}`
    console.log("New frame", this.frame)
  }

  ngOnInit(): void {
    this._animationService.currentFrameIndex
      .takeUntil(this._componentDestroyed.asObservable())
      .subscribe(index => {
        this._currentFrameIndex = index
        this.active = this._currentFrameIndex == this.frameIndex
      })
  }

  ngOnDestroy() {
    this._componentDestroyed.next(true);
    this._componentDestroyed.complete();    
  }
}
