import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AnimationPageComponent } from './animation-page/animation-page.component';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { AnimationFrameComponent } from './animation-frame/animation-frame.component';
import { AnimationComponent } from './animation/animation.component';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AnimationsModalComponent } from './animations-modal/animations-modal.component';
import { NewAnimationModalComponent } from './new-animation-modal/new-animation-modal.component';

@NgModule({
  declarations: [AnimationPageComponent, AnimationFrameComponent, AnimationComponent, AnimationsModalComponent, NewAnimationModalComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    FontAwesomeModule,
    FormsModule,
    NgbModule,
  ],
  providers: [
  ]
})
export class BuildModule { }
