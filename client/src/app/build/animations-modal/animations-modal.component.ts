import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { AnimationService } from '../animation.service';
import { Settings } from '../../interfaces/settings';
import { SettingsService } from '../../settings.service';

@Component({
  selector: 'app-animations-modal',
  templateUrl: './animations-modal.component.html',
  styleUrls: ['./animations-modal.component.scss']
})
export class AnimationsModalComponent implements OnInit, OnDestroy  {

  private _componentDestroyed: Subject<boolean> = new Subject<boolean>();  
  public animations
  public settings: Settings
  
  constructor(
    public activeModal: NgbActiveModal,      
    public settingsService: SettingsService,    
    private _animationService: AnimationService
  ) { }

  ngOnInit(): void {
    this.settingsService.settings.takeUntil(this._componentDestroyed.asObservable())
    .subscribe(settings => {
      this.settings = settings
    })
    this._animationService.getAnimations().then((animationsList) => {
      this.animations = animationsList.animations
    });
  }

  openAnimation(animation) {
    this.settings.animation.name = animation
    this.settingsService.updateSettings(this.settings).then(() => {}, (err) => {})
  }

  close() {
    this.activeModal.close()
  }

  ngOnDestroy() {
    this._componentDestroyed.next(true);
    this._componentDestroyed.complete();    
  }

}
