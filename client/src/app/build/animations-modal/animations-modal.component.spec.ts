import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AnimationsModalComponent } from './animations-modal.component';

describe('AnimationsModalComponent', () => {
  let component: AnimationsModalComponent;
  let fixture: ComponentFixture<AnimationsModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AnimationsModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AnimationsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
