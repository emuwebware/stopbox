import { Component, OnInit, OnDestroy } from '@angular/core';
import { FramesList } from '../../interfaces/frames-list';
import { AnimationService } from '../animation.service';
import { Subject } from 'rxjs';

@Component({
  selector: 'animation',
  templateUrl: './animation.component.html',
  styleUrls: ['./animation.component.scss']
})
export class AnimationComponent implements OnInit, OnDestroy {

  public framesList: FramesList
  private _componentDestroyed: Subject<boolean> = new Subject<boolean>();  
  private _frameRefreshInterval

  constructor(
    private aniService: AnimationService,      
  ) { }

  ngOnInit(): void {
    this.aniService.framesList.takeUntil(this._componentDestroyed.asObservable())
      .subscribe(framesList => {
        this.framesList = framesList
        this.aniService.goLast()
      });
    this.aniService.getFrames().then(() => {}, (err) => {});
    this.aniService.recording.takeUntil(this._componentDestroyed.asObservable())
      .subscribe(recording => {
        if(recording) {
          if(!this._frameRefreshInterval) {
            this._frameRefreshInterval = setInterval(() => {
              this.aniService.getFrames().then(() => {}, (err) => {});
            }, 1000);
          }
        } else {
          if(this._frameRefreshInterval) {
            clearInterval(this._frameRefreshInterval);
            this._frameRefreshInterval = null;
          }
        }
      });
  }

  ngOnDestroy() {
    this._componentDestroyed.next(true);
    this._componentDestroyed.complete();    
  }

}
