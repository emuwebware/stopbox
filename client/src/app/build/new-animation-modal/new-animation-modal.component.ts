import { Component, OnInit, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Subject } from 'rxjs';
import { AnimationService } from '../animation.service';
import { Settings } from '../../interfaces/settings';
import { SettingsService } from '../../settings.service';


@Component({
  selector: 'app-new-animation-modal',
  templateUrl: './new-animation-modal.component.html',
  styleUrls: ['./new-animation-modal.component.scss']
})
export class NewAnimationModalComponent implements OnInit, OnDestroy  {

  private _componentDestroyed: Subject<boolean> = new Subject<boolean>();  
  public animationName: string
  public settings: Settings
  
  constructor(
    public activeModal: NgbActiveModal,      
    public settingsService: SettingsService
  ) { }


  ngOnInit(): void {
    this.settingsService.settings.takeUntil(this._componentDestroyed.asObservable())
    .subscribe(settings => {
      this.settings = settings
    })
  }

  close() {
    this.activeModal.close()
  }

  createAnimation() {
    this.settings.animation.name = this.animationName
    this.settingsService.updateSettings(this.settings).then(() => {
      this.close()
    }, (err) => {})
  }

  ngOnDestroy() {
    this._componentDestroyed.next(true);
    this._componentDestroyed.complete();    
  }

}
