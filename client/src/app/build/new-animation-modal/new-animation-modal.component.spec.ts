import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewAnimationModalComponent } from './new-animation-modal.component';

describe('NewAnimationModalComponent', () => {
  let component: NewAnimationModalComponent;
  let fixture: ComponentFixture<NewAnimationModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewAnimationModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewAnimationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
