import { Component, OnInit, HostListener } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SettingsModalComponent } from '../../settings-modal/settings-modal.component';
import { faPlay, faStop, faStepBackward, faStepForward, faForward, faBackward, faCameraRetro, faCamera } from '@fortawesome/free-solid-svg-icons';
import { AnimationService } from '../animation.service';
import { Subject } from 'rxjs';
import { SettingsService } from '../../settings.service';
import { Settings } from '../../interfaces/settings';
import { AnimationsModalComponent } from '../animations-modal/animations-modal.component';
import { NewAnimationModalComponent } from '../new-animation-modal/new-animation-modal.component';

export enum KEY_CODE {
  RIGHT_ARROW = 39,
  LEFT_ARROW = 37
}

@Component({
  selector: 'animation-page',
  templateUrl: './animation-page.component.html',
  styleUrls: ['./animation-page.component.scss'],
})
export class AnimationPageComponent implements OnInit {

  private _componentDestroyed: Subject<boolean> = new Subject<boolean>();
  
  public faPlay = faPlay
  public faStop = faStop
  public faStepForward = faStepForward
  public faStepBackward = faStepBackward
  public faForward = faForward
  public faBackward = faBackward
  public faCamera = faCamera
  public playing: boolean
  public recording: boolean
  public settings: Settings

  @HostListener('window:keyup', ['$event'])
  handleCursorRight(event: KeyboardEvent) { 
    if (event.keyCode === KEY_CODE.RIGHT_ARROW) {
      this.stepForward()
    }
    if (event.keyCode === KEY_CODE.LEFT_ARROW) {
      this.stepBackward()
    }    
  }

  constructor(
    private _modalService: NgbModal,
    private _animationService: AnimationService,
    private _settingsService: SettingsService
  ) { }

  ngOnInit(): void {
    this._settingsService.settings.takeUntil(this._componentDestroyed.asObservable())
      .subscribe(settings => {
        this.settings = settings;
      })
    this._animationService.playing
      .takeUntil(this._componentDestroyed.asObservable())
      .subscribe(playing => {
        this.playing = playing
      })
    this._animationService.recording
      .takeUntil(this._componentDestroyed.asObservable())
      .subscribe(recording => {
        this.recording = recording
      })
  }

  updatePlaySpeed(interval) {
    this.settings.animation.interval = interval
    this._settingsService.updateSettings(this.settings).then(() => {
      if(this.playing) {
        this._animationService.stop()
        this._animationService.play()
      }
    });
  }

  newAnimation() {
    const newAnimationModalRef = this._modalService.open(NewAnimationModalComponent);    
  }

  openAnimation() {
    const animationsModalRef = this._modalService.open(AnimationsModalComponent, { size: 'lg' });    
  }

  goFirst() {
    this._animationService.goFirst()
  }
  
  goLast() {
    this._animationService.goLast()
  }

  startRecording() {
    this._animationService.startRecording()
  }
  
  stopRecording() {
    this._animationService.stopRecording()
  }

  stepForward() {
    this._animationService.stepForward()
  }
  
  stepBackward() {
    this._animationService.stepBackward()
  }
  
  play() {
    this._animationService.play()
  }
  
  stop() {
    this._animationService.stop()
  }

  showSettings() {
    const settingsModalRef = this._modalService.open(SettingsModalComponent, { size: 'lg' });
  }

  ngOnDestroy() {
    this._componentDestroyed.next(true);
    this._componentDestroyed.complete();    
  }

}
