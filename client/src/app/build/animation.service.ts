import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import 'rxjs-compat/add/operator/map';
import 'rxjs-compat/add/operator/filter';
import { FramesList } from '../interfaces/frames-list';
import { Observable, BehaviorSubject } from 'rxjs';
import { faStepBackward } from '@fortawesome/free-solid-svg-icons';
import { SettingsService } from '../settings.service';
import { AnimationsList } from '../interfaces/animations-list';



@Injectable({
  providedIn: 'root'
})
export class AnimationService {

  public currentFrameIndex: BehaviorSubject<number> = new BehaviorSubject(0)
  public playing: BehaviorSubject<boolean> = new BehaviorSubject(false)
  public recording: BehaviorSubject<boolean> = new BehaviorSubject(false)
  public playSpeed: BehaviorSubject<number> = new BehaviorSubject(200)
  public framesList: BehaviorSubject<FramesList> = new BehaviorSubject(null)
  public animations: BehaviorSubject<string[]> = new BehaviorSubject(null)
  private _framesList: FramesList
  private _animationsList: AnimationsList
  private _playInterval: any = null;
  
  constructor(
    public http: HttpClient,
    private _settingsService: SettingsService    
  ) {
    this._settingsService.settingsFile.filter(settingsFile => settingsFile !== null).subscribe(settingsFile => {
      this.getFrames().then(() => {
        this.currentFrameIndex.next(0)
      }, () => {});      
    })
    this._settingsService.settings.filter(settings => settings !== null).subscribe(settings => {
      if(settings.animation.interval) {
        this.playSpeed.next(settings.animation.interval);
      }
      this.getFrames().then(() => {
        this.currentFrameIndex.next(0)
      }, () => {});      
    });
  }

  public getFrames(): Promise<FramesList> {
    return this.http
      .get<FramesList>(`/api/animation/frames`)
      .map((response) => {
        this._framesList = response
        this.framesList.next(this._framesList)
        return this._framesList;
      }).toPromise();    
  }

  public getAnimations(): Promise<AnimationsList> {
    return this.http
      .get<AnimationsList>(`/api/animation/animations`)
      .map((response) => {
        this._animationsList = response
        this.animations.next(this._animationsList.animations)
        return this._animationsList;
      }).toPromise();    
  }

  public play() {
    this.playing.next(true)
    this._playInterval = setInterval(() => {
      this.stepForward();
    }, this.playSpeed.getValue())        
  }
  
  public stop() {
    if(this._playInterval) {
      this.playing.next(false)
      clearInterval(this._playInterval)
      this._playInterval = null;
    }
  }

  public startRecording() {
    this.recording.next(true);
    return this.http
      .get(`/api/animation/start-recording`).toPromise().then(() => {
        console.log("Recording started")
      }, (err) => {
        this.recording.next(false)
        console.log("Recording failed", err)
      });       
  }
  
  public stopRecording() {
    this.recording.next(false);
    return this.http
      .get(`/api/animation/stop-recording`).toPromise().then(() => {
        console.log("Recording stopped")
      }, (err) => {
        this.recording.next(true)
        console.log("Recording failed to stop", err)
      });       
  }

  public goFirst() {
    this.currentFrameIndex.next(0)
  }
  
  public goLast() {
    if(!(this._framesList && this._framesList.frames)) {
      return 
    }
    this.currentFrameIndex.next(this._framesList.frames.length - 1)
  }

  public stepForward() {
    let frameIndex = this.currentFrameIndex.getValue()
    if(frameIndex == null) {
      frameIndex = 0;
    } else {
      frameIndex++;
    }
    if(frameIndex == this._framesList.frames.length) {
      frameIndex = 0;
    }
    this.currentFrameIndex.next(frameIndex)
  }
  
  public stepBackward() {
    let frameIndex = this.currentFrameIndex.getValue()
    if(frameIndex == null) {
      frameIndex = 0;
    } else {
      frameIndex--;
    }
    if(frameIndex == -1) {
      frameIndex = this._framesList.frames.length - 1;
    }
    this.currentFrameIndex.next(frameIndex)
  }

}
