import { SettingsService } from 'src/global/settings/settings.service';
import { FramesList } from 'src/interfaces/frames-list.interface';
import { BehaviorSubject } from 'rxjs';
export declare class AnimationService {
    private _settingsService;
    recording: BehaviorSubject<boolean>;
    private _stopBoxProc;
    private _stopBoxScript;
    constructor(_settingsService: SettingsService);
    getAnimations(): string[];
    getFrames(): FramesList;
    stopRecording(): Promise<null>;
    startRecording(): Promise<null>;
}
