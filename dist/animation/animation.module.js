"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const animation_controller_1 = require("./animation/animation.controller");
const animation_service_1 = require("./animation.service");
const global_module_1 = require("../global/global.module");
let AnimationModule = class AnimationModule {
};
AnimationModule = __decorate([
    common_1.Module({
        controllers: [animation_controller_1.AnimationController],
        providers: [animation_service_1.AnimationService],
        imports: [global_module_1.GlobalModule]
    })
], AnimationModule);
exports.AnimationModule = AnimationModule;
//# sourceMappingURL=animation.module.js.map