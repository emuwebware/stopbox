"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const fs = require("fs");
const path = require("path");
const child_process = require("child_process");
const settings_service_1 = require("../global/settings/settings.service");
const frames_list_interface_1 = require("../interfaces/frames-list.interface");
const rxjs_1 = require("rxjs");
let AnimationService = class AnimationService {
    constructor(_settingsService) {
        this._settingsService = _settingsService;
        this.recording = new rxjs_1.BehaviorSubject(false);
        this._stopBoxScript = `${path.join(path.dirname(require.main.filename), '..', 'scripts')}/stopbox.py`;
    }
    getAnimations() {
        return fs.readdirSync(this._settingsService.outputDir).filter(file => fs.lstatSync(path.join(this._settingsService.outputDir, file)).isDirectory());
    }
    getFrames() {
        let frames = [];
        if (fs.existsSync(this._settingsService.framesPath)) {
            frames = fs.readdirSync(this._settingsService.framesPath).filter(file => {
                return ['.png', '.jpg'].find(ext => ext == path.extname(file)) && fs.statSync(this._settingsService.framesPath + "/" + file).size > 400000;
            });
        }
        return {
            path: this._settingsService.framesPath,
            frames: frames
        };
    }
    stopRecording() {
        return new Promise((resolve, reject) => {
            if (this._stopBoxProc) {
                child_process.execFileSync('sudo', ['kill', this._stopBoxProc.pid]);
                this._stopBoxProc = null;
            }
            this.recording.next(false);
            resolve();
        });
    }
    startRecording() {
        return new Promise((resolve, reject) => {
            console.log("Starting recording");
            let args = [
                "python", "-u", this._stopBoxScript, "--settings", this._settingsService.settingsFile.getValue()
            ];
            this._stopBoxProc = child_process.spawn('sudo', args, { detached: true });
            this._stopBoxProc.stdout.on('data', (data) => {
                data = String(data).replace(/[\n\r]+/g, '');
                console.log("stdout: " + data);
            });
            this.recording.next(true);
            resolve();
        });
    }
};
AnimationService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [settings_service_1.SettingsService])
], AnimationService);
exports.AnimationService = AnimationService;
//# sourceMappingURL=animation.service.js.map