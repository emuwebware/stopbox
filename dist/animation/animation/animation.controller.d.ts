import { AnimationService } from '../animation.service';
import { FramesList } from 'src/interfaces/frames-list.interface';
import { SettingsService } from '../../global/settings/settings.service';
interface RecordingResponse {
    recording: boolean;
}
interface AnimationsList {
    animations: string[];
}
export declare class AnimationController {
    private animationService;
    private settingsService;
    constructor(animationService: AnimationService, settingsService: SettingsService);
    getAnimations(): AnimationsList;
    getFrames(): FramesList;
    getFrame(params: any, res: any): FramesList;
    startRecording(): Promise<RecordingResponse>;
    stopRecording(): Promise<RecordingResponse>;
}
export {};
