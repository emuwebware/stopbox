"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const animation_service_1 = require("../animation.service");
const frames_list_interface_1 = require("../../interfaces/frames-list.interface");
const settings_service_1 = require("../../global/settings/settings.service");
let AnimationController = class AnimationController {
    constructor(animationService, settingsService) {
        this.animationService = animationService;
        this.settingsService = settingsService;
    }
    getAnimations() {
        return {
            animations: this.animationService.getAnimations()
        };
    }
    getFrames() {
        return this.animationService.getFrames();
    }
    getFrame(params, res) {
        return res.sendFile(`${this.settingsService.framesPath}/${params.filename}`);
    }
    startRecording() {
        return new Promise((resolve, reject) => {
            this.animationService.startRecording().then(() => {
                resolve({ recording: true });
            }, (err) => {
                throw new common_1.HttpException('Bad Request', common_1.HttpStatus.BAD_REQUEST);
            });
        });
    }
    stopRecording() {
        return new Promise((resolve, reject) => {
            this.animationService.stopRecording().then(() => {
                resolve({ recording: false });
            }, (err) => {
                throw new common_1.HttpException('Bad Request', common_1.HttpStatus.BAD_REQUEST);
            });
        });
    }
};
__decorate([
    common_1.Get('animations'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Object)
], AnimationController.prototype, "getAnimations", null);
__decorate([
    common_1.Get('frames'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Object)
], AnimationController.prototype, "getFrames", null);
__decorate([
    common_1.Get('frame/:filename'),
    __param(0, common_1.Param()), __param(1, common_1.Res()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Object)
], AnimationController.prototype, "getFrame", null);
__decorate([
    common_1.Get('start-recording'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AnimationController.prototype, "startRecording", null);
__decorate([
    common_1.Get('stop-recording'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", Promise)
], AnimationController.prototype, "stopRecording", null);
AnimationController = __decorate([
    common_1.Controller('animation'),
    __metadata("design:paramtypes", [animation_service_1.AnimationService,
        settings_service_1.SettingsService])
], AnimationController);
exports.AnimationController = AnimationController;
//# sourceMappingURL=animation.controller.js.map