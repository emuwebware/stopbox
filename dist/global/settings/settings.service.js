"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const fs = require("fs");
const ini = require("ini");
const path = require("path");
const settings_interface_1 = require("../../interfaces/settings.interface");
const BehaviorSubject_1 = require("rxjs/BehaviorSubject");
let SettingsService = class SettingsService {
    constructor() {
        this.settings = new BehaviorSubject_1.BehaviorSubject(null);
        this.settingsFile = new BehaviorSubject_1.BehaviorSubject(null);
        this.framesPath = null;
        this.outputDir = null;
        let settingsFile = `${path.join(path.dirname(require.main.filename), '..', 'scripts')}/settings.ini`;
        this.settingsFile.next(settingsFile);
        this.getSettings();
    }
    updateSettingsFile(settingsFile) {
        if (!fs.existsSync(settingsFile)) {
            return false;
        }
        this.settingsFile.next(settingsFile);
        this.getSettings();
        return true;
    }
    updateSettings(settings) {
        fs.writeFileSync(this.settingsFile.getValue(), ini.stringify(settings));
        this.getSettings();
    }
    getSettings() {
        let settingsFile = this.settingsFile.getValue();
        if (!settingsFile) {
            return;
        }
        if (!fs.existsSync(settingsFile)) {
            this._settings = null;
            this.settings.next(this._settings);
            return;
        }
        this._settings = ini.parse(fs.readFileSync(settingsFile, 'utf-8'));
        this._settings.animation.interval = parseInt(this._settings.animation.interval);
        this._setPaths();
        this.settings.next(this._settings);
    }
    _setPaths() {
        this.outputDir = this._settings.animation.output_dir;
        this.framesPath = path.join(this._settings.animation.output_dir, this._settings.animation.name);
    }
};
SettingsService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [])
], SettingsService);
exports.SettingsService = SettingsService;
//# sourceMappingURL=settings.service.js.map