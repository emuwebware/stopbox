import { SettingsService } from './settings.service';
import { Settings } from '../../interfaces/settings.interface';
interface SettingsFileResponse {
    settingsFile: string;
}
interface SettingsFileRequest {
    settingsFile: string;
}
export declare class SettingsController {
    private settingsService;
    constructor(settingsService: SettingsService);
    getSettings(): Settings;
    updateSettings(settings: Settings): Settings;
    getSettingsFile(): SettingsFileResponse;
    setSettingsFile(settingsFileRequest: SettingsFileRequest): SettingsFileResponse;
}
export {};
