import { Settings } from 'src/interfaces/settings.interface';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
export declare class SettingsService {
    private _settings;
    settings: BehaviorSubject<Settings>;
    settingsFile: BehaviorSubject<string>;
    framesPath: string;
    outputDir: string;
    constructor();
    updateSettingsFile(settingsFile: string): boolean;
    updateSettings(settings: Settings): void;
    getSettings(): void;
    private _setPaths;
}
