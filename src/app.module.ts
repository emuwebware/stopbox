import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { AnimationModule } from './animation/animation.module';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', '/client/dist/client'),
      exclude: ['/api*'],
    }),
    AnimationModule
  ],  
  controllers: [AppController],
})
export class AppModule {}

// /Users/clucani/code/stopbox/stopbox/client/dist/client/index.html