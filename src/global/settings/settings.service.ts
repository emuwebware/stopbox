import { Injectable } from '@nestjs/common';
import * as fs from 'fs';
import * as ini from 'ini';
import * as path from 'path';
import { Settings } from 'src/interfaces/settings.interface';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class SettingsService {

  private _settings: Settings
  public settings: BehaviorSubject<Settings> = new BehaviorSubject(null)
  public settingsFile: BehaviorSubject<string> = new BehaviorSubject(null)
  public framesPath: string = null
  public outputDir: string = null

  constructor() {
    let settingsFile = `${path.join(path.dirname(require.main.filename), '..', 'scripts')}/settings.ini`
    this.settingsFile.next(settingsFile)
    this.getSettings();
  }

  updateSettingsFile(settingsFile: string): boolean {
    if(!fs.existsSync(settingsFile)) {
      return false;
    }
    this.settingsFile.next(settingsFile)
    this.getSettings();
    return true;
  }

  updateSettings(settings: Settings) {
    fs.writeFileSync(this.settingsFile.getValue(), ini.stringify(settings))
    this.getSettings();
  }

  getSettings() {
    let settingsFile = this.settingsFile.getValue()
    if(!settingsFile) {
      return;
    }
    if(!fs.existsSync(settingsFile)) {
      this._settings = null
      this.settings.next(this._settings);
      return;
    }
    this._settings = ini.parse(fs.readFileSync(settingsFile, 'utf-8'))
    this._settings.animation.interval = parseInt(this._settings.animation.interval);
    this._setPaths()
    this.settings.next(this._settings);
  } 

  private _setPaths() {
    this.outputDir = this._settings.animation.output_dir
    this.framesPath = path.join(this._settings.animation.output_dir, this._settings.animation.name)
  }

}
