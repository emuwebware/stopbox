import { Controller, Get, Param, Res, Post, Req, Body, HttpException, HttpStatus } from '@nestjs/common';
import { SettingsService } from './settings.service';
import { Settings } from '../../interfaces/settings.interface';

interface SettingsFileResponse {
  settingsFile: string
}

interface SettingsFileRequest {
  settingsFile: string
}

@Controller('')
export class SettingsController {

  constructor(
    private settingsService: SettingsService
  ) {}

  @Get('settings')
  getSettings(): Settings {
    return this.settingsService.settings.getValue()
  }  

  @Post('settings')
  updateSettings(@Body() settings: Settings): Settings {
    this.settingsService.updateSettings(settings);
    return this.settingsService.settings.getValue()
  }  

  @Get('settings-file')
  getSettingsFile(): SettingsFileResponse {
    return <SettingsFileResponse>{
      settingsFile: this.settingsService.settingsFile.getValue()
    }
  }

  @Post('settings-file')
  setSettingsFile(@Body() settingsFileRequest: SettingsFileRequest) {
    if(this.settingsService.updateSettingsFile(settingsFileRequest.settingsFile)) {
      return <SettingsFileResponse>{
        settingsFile: this.settingsService.settingsFile.getValue()
      }    
    } else {
      throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
    }
  }

}
