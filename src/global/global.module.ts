import { Module } from '@nestjs/common';
import { SettingsService } from './settings/settings.service';
import { SettingsController } from './settings/settings.controller';

@Module({
  providers: [SettingsService],
  exports: [SettingsService],
  controllers: [SettingsController]
})
export class GlobalModule {}
