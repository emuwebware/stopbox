import { Controller, Get, Param, Res, HttpException, HttpStatus } from '@nestjs/common';
import { AnimationService } from '../animation.service';
import { FramesList } from 'src/interfaces/frames-list.interface';
import { SettingsService } from '../../global/settings/settings.service';

interface RecordingResponse {
  recording: boolean
}

interface AnimationsList {
  animations: string[]
}

@Controller('animation')
export class AnimationController {
  constructor(
    private animationService: AnimationService,
    private settingsService: SettingsService
  ) {}

  @Get('animations')
  getAnimations(): AnimationsList {
    return {
      animations: this.animationService.getAnimations()
    }
  }

  @Get('frames')
  getFrames(): FramesList {
    return this.animationService.getFrames()
  }  

  @Get('frame/:filename')
  getFrame(@Param() params, @Res() res): FramesList {
    return res.sendFile(`${this.settingsService.framesPath}/${params.filename}`);
  }  

  @Get('start-recording')
  startRecording(): Promise<RecordingResponse>  {
    return new Promise((resolve, reject) => {
      this.animationService.startRecording().then(() => {
        resolve({ recording: true })
      }, (err) => {
        throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
      })
    });
  }  
  
  @Get('stop-recording')
  stopRecording(): Promise<RecordingResponse> {
    return new Promise((resolve, reject) => {
      this.animationService.stopRecording().then(() => {
        resolve({ recording: false })
      }, (err) => {
        throw new HttpException('Bad Request', HttpStatus.BAD_REQUEST);
      })
    });
  }  


}