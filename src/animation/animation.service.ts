import { Injectable } from '@nestjs/common';
import * as fs from 'fs';
import * as path from 'path';
import * as child_process from 'child_process'
import { SettingsService } from 'src/global/settings/settings.service';
import { FramesList } from 'src/interfaces/frames-list.interface';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class AnimationService {

  public recording: BehaviorSubject<boolean> = new BehaviorSubject(false)
  private _stopBoxProc: any
  private _stopBoxScript: string

  constructor(
    private _settingsService: SettingsService
  ) {
    this._stopBoxScript = `${path.join(path.dirname(require.main.filename), '..', 'scripts')}/stopbox.py`
  }

  getAnimations(): string[] {
    return fs.readdirSync(this._settingsService.outputDir).filter(file => fs.lstatSync(path.join(this._settingsService.outputDir, file)).isDirectory())
  }

  getFrames(): FramesList {
    let frames: string[] = []
    if(fs.existsSync(this._settingsService.framesPath)) {
      frames = fs.readdirSync(this._settingsService.framesPath).filter(file => {
        return ['.png', '.jpg'].find(ext => ext == path.extname(file)) && fs.statSync(this._settingsService.framesPath + "/" + file).size > 400000
      })
    }
    return <FramesList>{
      path: this._settingsService.framesPath,
      frames: frames 
    }
  }

  stopRecording(): Promise<null> {
    return new Promise((resolve, reject) => {
      if(this._stopBoxProc) {
        child_process.execFileSync('sudo', ['kill', this._stopBoxProc.pid]);
        this._stopBoxProc = null
      }
      this.recording.next(false)
      resolve()
    });
  }
  
  startRecording(): Promise<null> {
    return new Promise((resolve, reject) => {
      console.log("Starting recording");
      let args = [
        "python", "-u", this._stopBoxScript, "--settings", this._settingsService.settingsFile.getValue()
      ];
      this._stopBoxProc = child_process.spawn('sudo', args, { detached: true })

      this._stopBoxProc.stdout.on('data', (data) => {
        data = String(data).replace(/[\n\r]+/g, '');
        console.log("stdout: " + data)
        // let dataPart = data.split("|");
        // if(dataPart.length >= 2) {
        //   let dataType = dataPart[0];
        //   let dataValue = dataPart[1];
        //   io.emit(`capture:${dataType.trim()}`, dataValue);
        //   // if(module.iniWritten == false) {
        //   //   configModule.updateConfig(app.caviConfig, `${app.caviConfig.sequencePath}/config.ini`);
        //   //   module.iniWritten = true;
        //   // }
        //}
      });      
      
      // console.log(path.dirname(require.main.filename));
      // console.log(JSON.stringify(args));
      // console.log(this._stopBoxScript);

      this.recording.next(true)
      resolve()
    });
  }

}
