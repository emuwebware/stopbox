import { Module } from '@nestjs/common';
import { AnimationController } from './animation/animation.controller';
import { AnimationService } from './animation.service';
import { GlobalModule } from 'src/global/global.module';

@Module({
  controllers: [AnimationController],
  providers: [AnimationService],
  imports: [GlobalModule]  
})
export class AnimationModule {}
